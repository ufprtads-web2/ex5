<%-- 
    Document   : erro
    Created on : Sep 17, 2018, 11:08:07 PM
    Author     : cassiano
--%>
<%@page isErrorPage="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="templates/meta.html" %>  
        <%@ include file="templates/header.html" %>  
    </head>
    <body>        
        Message:
        ${pageContext.exception.message}

        StackTrace:
        ${pageContext.out.flush()}
        ${pageContext.exception.printStackTrace(pageContext.response.writer)}
        
    <jsp:include page="templates/footer.jsp"></jsp:include>
    <%@include file="templates/js_scripts.html" %>           
    </body>
</html>
