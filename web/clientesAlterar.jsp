<%-- 
    Document   : clientesListar
    Created on : Sep 18, 2018, 2:20:50 PM
    Author     : cassiano
--%>
<%@page import="com.ufpr.tads.web2.beans.LoginBean"%>
<%  
    String errorMsg = "Usuário deve se autenticar para acessar o sistema";    
    LoginBean usuario = (LoginBean)session.getAttribute("login");        
    if (usuario == null){
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");                        
        request.setAttribute("errorMsg",errorMsg);        
//        response.sendRedirect("index.jsp?errorMsg=daledale");
        rd.forward(request, response);
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/templates/meta.html"%>
    <%@include file="/templates/header.html"%>
</head> 
    <%@include file="/templates/style.html"%>
<body> 
    <%@include file="/templates/navbar.html"%>
    <div class="w3-main" style="margin-left:340px;margin-right:40px">
    <h1 class="w3-jumbo"><b>Alterar Clientes</b></h1>
    <h1 class="w3-xxxlarge w3-text-red"><b>Clientes</b></h1>
    <form action="/web2ex5/AlterarClienteServlet" method="POST">        
          Nome:<br/><input type='text' name="nome" value=${cliente.nome}><br/>
          CPF:<br/><input type='text' name="cpf" value=${cliente.cpf}><br/>
          Email:<br/><input type='text' name="email" value=${cliente.email}><br/>
          Rua:<br/><input type='text' name="rua" value=${cliente.rua}><br/>
          NR:<br/><input type='text' name="nr" value=${cliente.nr}><br/>
          CEP:<br/><input type='text' name="cep" value=${cliente.cep}><br/>
          Cidade:<br/><input type='text' name="cidade" value=${cliente.cidade}><br/>
          UF:<br/><input type='text' name="uf" value=${cliente.uf}><br/>
          ID:<br/><input type='text' name="id" readonly="readonly" value=${cliente.id}><br/>
          <input type='submit' value='Alterar'/>
    </form>
          <a href="/ClientesServlet" style="color: red">Retornar</a>
    </div>
    <%@include file="/templates/footer.jsp"%>
    <%@include file="/templates/js_scripts.html"%>
</body>
</html>
