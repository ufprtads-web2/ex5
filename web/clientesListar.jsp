<%-- 
    Document   : clientesListar
    Created on : Sep 18, 2018, 2:20:50 PM
    Author     : cassiano
--%>
<%@page import="com.ufpr.tads.web2.beans.LoginBean"%>
<%  
    String errorMsg = "Usuário deve se autenticar para acessar o sistema";    
    LoginBean usuario = (LoginBean)session.getAttribute("login");        
    if (usuario == null){
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");                        
        request.setAttribute("errorMsg",errorMsg);        
//        response.sendRedirect("index.jsp?errorMsg=daledale");
        rd.forward(request, response);
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/templates/meta.html"%>
    <%@include file="/templates/header.html"%>
</head> 
    <%@include file="/templates/style.html"%>
<body> 
    <%@include file="/templates/navbar.html"%>
    <div class="w3-main" style="margin-left:340px;margin-right:40px">
    <h1 class="w3-jumbo"><b>Listar Clientes</b></h1>
    <h1 class="w3-xxxlarge w3-text-red"><b>Clientes</b></h1>
    <table id="t01" class=display>
    <thead>
    <tr>
      <th>Nome</th>
      <th>CPF</th>
      <th>ID</th>      
      <th>Opções</th>
    </tr>
  </thead>
    <tbody>
    <c:forEach items="${requestScope.clientes}" var="cli">
        <tr>
        <td>${cli.nome}</td>
        <td>${cli.cpf}</td>
        <td>${cli.email}</td>
        <td>            
        <a href=ClientesServlet?action=SHOW&id=${cli.id} style="color: red">Visualizar</a>                
        <a href=ClientesServlet?action=FORMUPDATE&id=${cli.id} style="color: red">Alterar</a>
        <a href=ClientesServlet?action=REMOVE&id=${cli.id} style="color: red">Remover</a>
        </td>        
        </tr>
        </c:forEach>          
  </tbody>  
</table>     
    </div>
    <%@include file="/templates/footer.jsp"%>
    <%@include file="/templates/js_scripts.html"%>
</body>
</html>
