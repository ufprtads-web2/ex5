/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.servlets;

import com.ufpr.tads.web2.beans.Cliente;
import com.ufpr.tads.web2.beans.LoginBean;
import com.ufpr.tads.web2.dao.ClienteDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author cassiano
 */
@WebServlet(name = "ClientesServlet", urlPatterns = {"/ClientesServlet"})
public class ClientesServlet extends HttpServlet {
    
    private String errorMsg = "Usuário deve se autenticar para acessar o sistema";
    private String id;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {     
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");        
        try {
            HttpSession session = request.getSession();
            LoginBean usuario = (LoginBean)session.getAttribute("login");        
            if (usuario == null){                
                request.setAttribute("errorMsg",errorMsg);        
                rd.forward(request, response);
            }
            String action = (String) request.getAttribute("action");
            ClienteDAO dao = new ClienteDAO();
            Cliente cliente = new Cliente();
            if (action == null){
                action = "LIST";
            }
            switch(action) {
                case "LIST":
                    List<Cliente> resultado = dao.selectClientes();
                    rd = getServletContext().getRequestDispatcher("/clientesListar.jsp");
                    request.setAttribute("clientes", resultado);
                    rd.forward(request, response);
                    break;
                case "SHOW":
                    id = request.getParameter("id");
                    rd = getServletContext().getRequestDispatcher("/clientesVisualizar.jsp");                        
                    if (id != null){
                        Cliente cli = dao.selectCliente(id);                        
                        request.setAttribute("cliente", cli);
                        rd.forward(request, response);
                    }
                    break;
                case "FORMUPDATE":
                    rd = getServletContext().getRequestDispatcher("/clientesAlterar.jsp");                        
                    if (id != null){
                        id = request.getParameter("id");
                        Cliente cli = dao.selectCliente(id);                        
                        request.setAttribute("cliente", cli);
                        rd.forward(request,response);
                    }
                case "REMOVE":
                    rd = getServletContext().getRequestDispatcher("/ClientesServlet");                        
                    if (id != null){
                        id = request.getParameter("id");
                        dao.removeCliente(id);
                        rd.forward(request,response);
                    }
                case "UPDATE":
                    rd = getServletContext().getRequestDispatcher("/ClientesServlet.jsp");                                                                
                    int id_cliente = Integer.parseInt(request.getParameter("id"));
                    cliente.setId(id_cliente);
                    cliente.setNome(request.getParameter("nome"));
                    cliente.setCpf(request.getParameter("cpf"));
                    cliente.setEmail(request.getParameter("email"));
                    cliente.setCidade(request.getParameter("cidade"));
                    cliente.setUf(request.getParameter("uf"));
                    cliente.setCep(request.getParameter("cep"));
                    cliente.setRua(request.getParameter("rua"));
                    int nr = Integer.parseInt(request.getParameter("nr"));
                    cliente.setNr(nr);       
                    dao.updateCliente(cliente);
                    rd.forward(request, response);
                case "FORMNEW":
                    rd = getServletContext().getRequestDispatcher("/clientesNovo.jsp");                                            
                    rd.forward(request, response);
                case "new":                    
                    rd = getServletContext().getRequestDispatcher("/ClientesServlet");                                        
                    cliente.setNome(request.getParameter("nome"));
                    cliente.setCpf(request.getParameter("cpf"));
                    cliente.setEmail(request.getParameter("email"));
                    cliente.setCidade(request.getParameter("cidade"));
                    cliente.setUf(request.getParameter("uf"));
                    cliente.setCep(request.getParameter("cep"));
                    cliente.setRua(request.getParameter("rua"));
                    int ener = Integer.parseInt(request.getParameter("nr"));
                    cliente.setNr(ener);       
                    dao.insertCliente(cliente);
                    rd.forward(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClientesServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
