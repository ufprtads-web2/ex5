<%-- 
    Document   : portal
    Created on : Sep 17, 2018, 10:36:33 PM
    Author     : cassiano
--%>
<%@page import="com.ufpr.tads.web2.beans.LoginBean"%>
<%  
    String errorMsg = "Usu�rio deve se autenticar para acessar o sistema";    
    LoginBean usuario = (LoginBean)session.getAttribute("login");        
    if (usuario == null){
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");                        
        request.setAttribute("errorMsg",errorMsg);        
//        response.sendRedirect("index.jsp?errorMsg=daledale");
        rd.forward(request, response);
    }
%>
<!DOCTYPE html>
<jsp:useBean id="login" class="com.ufpr.tads.web2.beans.LoginBean"
scope="session"/>
<html>
    <head>
    <%@include file="templates/meta.html"%>
    <%@include file="templates/header.html"%>        
    </head>
    <%@ include file="templates/style.html" %>  
    <body>
    
    <%@include file="templates/navbar.html"%>                                
    <h1 align='right'>Bem vindo, <jsp:getProperty name="login" property="login"/></h1>
    <%@include file="templates/footer.jsp"%>            
    <%@include file="/templates/js_scripts.html" %>        
    </body>
</html>
